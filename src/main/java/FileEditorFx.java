import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;

/**
 * Created by Saturn on 10.09.2017.
 */
public class FileEditorFx extends Application {

   public static void main(String[] args) {
            Application.launch();
        }

        //bud. aplikacje
        public void start(Stage primaryStage) throws Exception {
            Parent scenGraph = createSceneGraph();
            Scene scene = new Scene(scenGraph);
            primaryStage.setScene(scene);

            primaryStage.show();
        }

        private Parent createSceneGraph() throws IOException {
            //return new Label("This is Java FX app");
            return FXMLLoader.load(getClass().getResource("editor.fxml"));
        }




}
