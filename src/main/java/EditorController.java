import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Saturn on 10.09.2017.
 */
public class EditorController {

    @FXML
    public TextArea textArea;

    @FXML
    public Button saveButton;
    public Button cancelButton;

    public void saveFile(ActionEvent actionEvent) {
        System.out.println("Save file");
        saveFile(textArea.getText());
    }

    @FXML
    private void initialize() {
        textArea.setText(readFile());
    }


    private void saveFile(String fileContent){
        //Get the file reference
        Path path = Paths.get("./src/main/java/editedFile.txt");

//Use try-with-resource to get auto-closeable writer instance
        try (BufferedWriter writer = Files.newBufferedWriter(path))
        {
            writer.write(fileContent);
        } catch(Exception ex){
            System.out.println("Error writing file");
        }

    }



    private String readFile() {
        Path homeDir = Paths.get(".", "src", "main", "java"); //("C:","work");
        Path pathToFile = homeDir.resolve("editedFile.txt");

        List<String> fileLines = null;

        StringBuffer sb = new StringBuffer("");

        try

        {
            fileLines = Files.readAllLines(pathToFile, Charset.forName("CP1250"));
            for (String fileLine : fileLines) {

                sb.append(fileLine + "\n");
            }
        } catch (
                IOException e)

        {
            System.out.println("File reading failed");
            e.printStackTrace();
        }
        return sb.toString();
    }

    public void cancel(ActionEvent actionEvent) {
        Platform.exit();
    }
}
